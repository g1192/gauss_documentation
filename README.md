

# Gauss Documentation

In this repository all the documentation about the gauss accounting project is stored.

# What is Gauss?

Gauss is a system created to manage the accounting of small to large-scale companies, it is a system that works on the different fronts of a business.

Thus performing in the management of:

- Personal
- Sales
- Products
- Customers
- Orders

Gauss will offer different interfaces for interaction with the client, employees, business managers and other systems, thus achieving that the user has an automation of many processes from invoicing to stock management based on their sales history.
In addition to this, this system will have direct interoperability with the Colombian treasury through electronic invoicing that is currently required by the regulations established by the DIAN (National Tax and Customs Directorate) in Colombia.

# Why gauss?

The name Gauss was chosen, in honor of Carl Friedrich Gauss. 

German mathematician, astronomer, and physicist who inspired me during my life, especially during my university career, because as the story tells, he was a person who was born in an adverse situation and that even so with the resources he had at his disposal, he achieved an huge impact in many areas of science and in history.

> _Regardless of where we start, we can all chart a path that changes history._

# Contact

Email:      felipepineda1997@gmail.com

Tel:        +57 305 3063835

WhatsApp:   +57 310 790 7321

# Documentación de Gauss

En este repositorio se almacena toda la documentación sobre el proyecto de contabilidad de gauss..

# ¿Que es Gauss?

Gauss es un sistema creado para manejar la contabilidad de empresas de pequeña a gran escala, es un sistema que se ejecutara en los diferentes frentes de un negocio. 

Desempeñándose así en la gestión de:

- Personal
- Ventas
- Productos
- Clientes
- Pedidos

Gauss ofrecerá distintas interfaces para la interacción con el cliente, empleados, jefes de negocio y otros sistemas, logrando así que el usuario tenga una automatización de muchos procesos desde la facturación hasta gestión del stock basado en su historial de venta.
Adicional a esto, este sistema tendrá interoperabilidad directa con el fisco colombiano mediante la facturación electrónica que actualmente es requerida por la normativa establecida por la DIAN (Dirección de Impuestos y Aduanas Nacionales) en Colombia.

# ¿Por qué Gauss?

Se eligió el nombre Gauss, en honor a Carl Friedrich Gauss.

Matemático, astrónomo y físico alemán que me inspiró durante mi vida, especialmente durante mi carrera universitaria, porque como cuenta la historia, era una persona que nació en una situación adversa y que aun así con los recursos que tenía a su disposición, logró un gran impacto en muchas áreas de la ciencia y en la historia.

> _Independientemente de dónde comencemos, todos podemos trazar un camino que cambie la historia._

# Contacto

Correo Electronico:     felipepineda1997@gmail.com

Tel:                    +57 305 3063835

WhatsApp:               +57 310 790 7321
